import 'bootstrap/dist/css/bootstrap.min.css';

import { Route, Routes } from 'react-router-dom';

import { Footer } from './components/Footer/Footer';
import { Header } from './components/Navbar/Header';
import { Home } from './pages/Home';
import { Personaje } from './pages/Personaje';

function App() {
  return (
    <div className='App'>
      <Header />

      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/personaje/:personajeId' element={<Personaje />} />
      </Routes>

      <Footer />
    </div>
  );
}

export default App;
