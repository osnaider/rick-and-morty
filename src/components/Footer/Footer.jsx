export function Footer() {
  return (
    <div className='navbar-dark bg-dark fixed-bottom'>
      <p className='text-center text-white py-2 mb-0'>
        Proyecto creado por el CAR 4 - 11/02/2022
      </p>
    </div>
  );
}
