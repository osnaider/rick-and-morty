import { EpisodioItem } from './EpisodioItem';

export function ListadoEpisodio({ datos }) {
  return (
    <div className='row'>
      {datos.map((episodio) => {
        let { id } = episodio;
        return <EpisodioItem {...episodio} key={id} />;
      })}
    </div>
  );
}
