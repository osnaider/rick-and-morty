// import bannerImg from '../../assets/img/banner-img.png';

export function Banner() {
  return (
    <div>
      <img
        style={{ height: '740px', objectFit: 'cover' }}
        src='src/assets/img/banner-img.png'
        className='card-img'
        alt='banner'
      />
    </div>
  );
}
